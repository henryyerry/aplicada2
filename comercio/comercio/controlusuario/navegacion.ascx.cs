﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace comercio.controlusuario
{
    public partial class navegacion : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if(!IsPostBack)
            {
                TreeNode hijo1 = new TreeNode("Animales");
                
                TreeView1.Nodes.Add(hijo1);

                TreeNode hijo2 = new TreeNode("Articulos");
                TreeView1.Nodes.Add(hijo2);
                TreeNode hijo3 = new TreeNode("Personas");
                TreeView1.Nodes.Add(hijo3);

                TreeNode agregaranimales = new TreeNode("Agregar animal");
                agregaranimales.NavigateUrl = "~/animales/addanimales.aspx";
                hijo1.ChildNodes.Add(agregaranimales);
                TreeNode agregarcategoria = new TreeNode("Agregar categoria");
                agregarcategoria.NavigateUrl = "~/animales/addcategoria.aspx";
                hijo1.ChildNodes.Add(agregarcategoria);



                TreeNode agregararticulo = new TreeNode("Agregar artículo");
                agregararticulo.NavigateUrl = "~/articulos/addarticulo.aspx";
                hijo2.ChildNodes.Add(agregararticulo);

                TreeNode agregarpersona = new TreeNode("Agregar persona");
                agregarpersona.NavigateUrl = "~/personas/add.aspx";
                hijo3.ChildNodes.Add(agregarpersona);
                TreeNode agregarcliente = new TreeNode("Agregar cliente");
                agregarcliente.NavigateUrl = "~/personas/addcliente.aspx";
                hijo3.ChildNodes.Add(agregarcliente);
                TreeNode agregarusuario = new TreeNode("Agregar usuario");
                agregarusuario.NavigateUrl = "~/personas/addusuario.aspx";
                hijo3.ChildNodes.Add(agregarusuario);

            }

        }

        protected void TreeView1_SelectedNodeChanged(object sender, EventArgs e)
        {

        }
    }
}