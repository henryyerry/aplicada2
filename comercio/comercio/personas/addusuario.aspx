﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="addusuario.aspx.cs" Inherits="comercio.personas.addusuario" %>

<%@ Register src="../controlusuario/navegacion.ascx" tagname="navegacion" tagprefix="uc1" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">
        .auto-style1 {
            width: 67%;
        }
        .auto-style2 {
            width: 234px;
        }
        #TextArea1 {
            width: 268px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    
        <table class="auto-style1">
            <tr>
                <td class="auto-style2" rowspan="5">
                    <uc1:navegacion ID="navegacion1" runat="server" />
                </td>
                <td class="auto-style2">Seleccionar:</td>
                <td>
                    <asp:DropDownList ID="DropDownList1" runat="server">
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td class="auto-style2">&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td class="auto-style2">Sueldo :</td>
                <td>
                    <asp:TextBox ID="SueldoUsuario" runat="server"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="auto-style2">Fecha de Inicio : </td>
                <td>
                    <asp:Calendar ID="Calendar1" runat="server"></asp:Calendar>
                </td>
            </tr>
            <tr>
                <td class="auto-style2">Dirección de Facturación :</td>
                <td>
                    <textarea id="TextArea1" name="S1" rows="2"></textarea></td>
            </tr>
        </table>
    
    </div>
    </form>
</body>
</html>
