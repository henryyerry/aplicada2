﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="addcliente.aspx.cs" Inherits="comercio.personas.addcliente" %>

<%@ Register src="../controlusuario/navegacion.ascx" tagname="navegacion" tagprefix="uc1" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">
        .auto-style1 {
            width: 81%;
        }
        .auto-style2 {
            text-align: center;
        }
        .auto-style3 {
            text-align: left;
        }
        .auto-style4 {
            height: 52px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    
        <table class="auto-style1">
            <tr>
                <td class="auto-style2" rowspan="4">
                    <uc1:navegacion ID="navegacion1" runat="server" />
                </td>
                <td class="auto-style2" colspan="2">CLIENTE</td>
            </tr>
            <tr>
                <td class="auto-style4">
                    <asp:Label ID="Label1" runat="server" Text="Seleccione un Cliente"></asp:Label>
                </td>
                <td class="auto-style4">
                    <asp:DropDownList ID="DropDownList1" runat="server" Height="25px" Width="243px">
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td class="auto-style3">
                    <asp:Label ID="Label2" runat="server" Text="Ingrese el Contacto"></asp:Label>
                </td>
                <td class="auto-style3">
                    <asp:TextBox ID="TextBox1" runat="server" Height="19px" Width="233px"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="auto-style2" colspan="2">
                    <asp:Button ID="Button1" runat="server" Text="Enviar..." />
                </td>
            </tr>
        </table>
    
    </div>
    </form>
</body>
</html>
